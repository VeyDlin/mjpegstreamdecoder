﻿using System;
using System.Drawing;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace JpegGet {
    class MjpegStreamDecoder {

        // JPEG разделители
        const byte picMarker = 0xFF;
        const byte picStart = 0xD8;
        const byte picEnd = 0xD9;

        // Токен
        private CancellationTokenSource cts;


        // Данные авторизации
        public string sourceUrl;
        public string login = null;
        public string password = null;

        // Размеры буферов
        public int chunkMaxSize = 1024;
        public int frameBufferSize = 1024 * 1024;


        // Контейнер для изображения
        public Action<Image> imageAction;


        public MjpegStreamDecoder() {
            cts = new CancellationTokenSource(); //  Cts.Cancel();
        }


      


        // Начать прием MJPEG стрима по http
        public async Task Start() {
            using (var cli = new HttpClient()) {

                if (!string.IsNullOrEmpty(login) && !string.IsNullOrEmpty(password)) {
                    cli.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.ASCII.GetBytes($"{login}:{password}")));
                }

                using (var stream = await cli.GetStreamAsync(sourceUrl).ConfigureAwait(false)) {
                    var streamBuffer = new byte[chunkMaxSize]; // Чанк буфер
                    var frameBuffer = new byte[frameBufferSize]; // Фрейм буфер
                    var frameIdx = 0; // Последнее записанное местоположение байта в буфере кадра
                    var inPicture = false; // Мы сейчас разбираем картинку?
                    byte current = 0x00; // Последний прочитанный байт
                    byte previous = 0x00; // Предпоследний прочитанный байт

                    // Вечно крутимся. Для выхода используется токен
                    while (true) {
                        var streamLength = await stream.ReadAsync(streamBuffer, 0, chunkMaxSize, cts.Token).ConfigureAwait(false);
                        parseStreamBuffer(frameBuffer, ref frameIdx, streamLength, streamBuffer, ref inPicture, ref previous, ref current);
                    };
                }
            }
        }





        // Разбор потокового буфера
        void parseStreamBuffer(byte[] frameBuffer, ref int frameIdx, int streamLength, byte[] streamBuffer, ref bool inPicture, ref byte previous, ref byte current) {
            var idx = 0;
            while (idx < streamLength) {
                if (inPicture) {
                    parsePicture(frameBuffer, ref frameIdx, ref streamLength, streamBuffer, ref idx, ref inPicture, ref previous, ref current);
                } else {
                    searchPicture(frameBuffer, ref frameIdx, ref streamLength, streamBuffer, ref idx, ref inPicture, ref previous, ref current);
                }
            }
        }





        // Поиск FFD8 (конца JPEG)
        void searchPicture(byte[] frameBuffer, ref int frameIdx, ref int streamLength, byte[] streamBuffer, ref int idx, ref bool inPicture, ref byte previous, ref byte current) {
            do {
                previous = current;
                current = streamBuffer[idx++];

                // Начало JPEG?
                if (previous == picMarker && current == picStart) {
                    frameIdx = 2;
                    frameBuffer[0] = picMarker;
                    frameBuffer[1] = picStart;
                    inPicture = true;
                    return;
                }
            } while (idx < streamLength);
        }





        // Пока мы парсим картинку, заполняем буфер кадров до достижения FFD9
        void parsePicture(byte[] frameBuffer, ref int frameIdx, ref int streamLength, byte[] streamBuffer, ref int idx, ref bool inPicture, ref byte previous, ref byte current) {
            do {
                previous = current;
                current = streamBuffer[idx++];
                frameBuffer[frameIdx++] = current;

                // Конец JPEG?
                if (previous == picMarker && current == picEnd) {
                    Image img = null;

                    // Использование MemoryStream таким образом предотвращает копирование массивов
                    using (var s = new MemoryStream(frameBuffer, 0, frameIdx)) {
                        try {
                            img = Image.FromStream(s);
                        } catch {
                            // Нас не волнуют плохо декодированные картинки
                        }
                    }

                    // Отложить обработку изображения, чтобы предотвратить замедление
                    // Делегат обработки изображения должен в конечном итоге избавиться от изображения.
                    Task.Run(() => imageAction(img));
                    inPicture = false;
                    return;
                }
            } while (idx < streamLength);
        }
    }
}
