﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace JpegGet {
    public partial class MainForm : Form {

        readonly SynchronizationContext sync;
        MjpegStreamDecoder streamer;

        public MainForm() {
            InitializeComponent();

            sync = SynchronizationContext.Current;

            streamer = new MjpegStreamDecoder();
            streamer.sourceUrl = "http://192.168.1.148:81/stream";
            streamer.imageAction = image => sync.Post(new SendOrPostCallback(_ => pictureBox1.Image = image), null);
        }



        async Task startVideoAsync() {
            await streamer.Start();
        }



        private void button1_Click(object sender, EventArgs e) {
            Task.Run(() => startVideoAsync());
        }
    }
}
